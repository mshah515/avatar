print("The Avatar: Aang")

qualities = ["Air Bender", "Funny", "Child-like", "Kind", "Strong", "Gentle", "Vegetarian", "Centenarian"]

print("Here are all of Aang's incredible qualities: ")

for q in qualities:
    print(q)
